# Hello Proto Example

### About

This is a git repo containing a copy of the Go GRPC example project.

I did this as the Go documentation relies on you using the already built example
inside the $GOPATH installation directory, which is fine, but ultimately results in you
missing a lot of knowledge about creating a go project from scratch.

## Go Path
When you install go, if no path is supplied it will default to $HOME. 

This is fine, however for some reason Go did not default to looking there for packages when i ran Go run via
the command line. 

So ensure you do:

```
export GOPATH=~/go/
```

Then add your GOPATH to your path:

```
export PATH=$PATH:$GOPATH/bin
```

When you run *go run* your installted modules should now be found.


## Imports

One issue with writing Go, is it's lack of support for relative or absolute imports.

Go only allows you to import from Packages, which are grouls of .go files.

To run a go program, the *main* function of the code, must be inside a *main* package.

From disucssion on the Go Slack channel, the best way to avoid common import erros with other go modules,
it is best to work in $GOPATH/src, where go will look for your modules correctly.

## Protobuf Generation

To use Googles protbufs, you need to have protoc installed, which is the transpiler that
converts protobuf files into Go code.

This is a 2 step process:

1) You need t install the c++ binary for your system, if you are using linux:

Download the *linux-86_64* zip from [here](https://github.com/protocolbuffers/protobuf/releases)

Extract the folder and then move protoc into usr/bin and google into usr/include

```
mv bin /usr/bin
```
```
mv include /usr/include
```

2) Install the go tool that will call protoc from GO:

```
go get -u github.com/golang/protobuf/protoc-gen-go
```

To compile the protobufs for this project you need to run:

```
 protoc -I hello_world_proto/ hello_world_proto/helloworld.proto --go_out=plugins=grpc:hello_world_proto
```

### Running the example

Once you have generated the protobufs, you then need to installed grpc:

```
go get -u google.golang.org/grpc
```

Then in one terminal start the server:

```
go run greeter_server/main.go
```

In another terminal send a greet to the localhost server:

```
go run greeter_client/main.go
```

You should get a response from the server and the program should exited cleanly.