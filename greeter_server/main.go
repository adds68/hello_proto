package main

import (
	"context"
	"log"
	"net"

	"google.golang.org/grpc"
	pb "grpc/hello_world_proto"
)

const (
	port = ":50051"
)

type server struct{}

func (s *server) SayHello(ctx context.Context, in *pb.HelloRequest) (*pb.HelloReply, error) {
	log.Printf("Recieved: %v", in.Name)
	return &pb.HelloReply{Message: "Hiya"}, nil
}

func main() {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatal("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterGreeterServer(s, &server{})
	if err := s.Serve(lis); err != nil {
		log.Fatal("failed to serve %v", err)
	}
}
