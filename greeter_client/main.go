package main

import (
	"context"
	"log"
	"os"
	"time"

	"google.golang.org/grpc"	
	pb "grpc/hello_world_proto"
)

const (
	address = "localhost:50051"
	defaultName = "world"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatal("Could not diat %v", err)
	}
	defer conn.Close()
	c := pb.NewGreeterClient(conn)

	name := defaultName
	if len(os.Args) > 1 {
		name = os.Args[1]
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.SayHello(ctx, &pb.HelloRequest{Name: name})
	if err != nil {
		log.Fatal("no greeting found %v", err)
	}
	log.Printf("greetings! %s", r.Message)
}

